#!/bin/bash

# Wait till ngrok tunnels is up and get https one
echo "Waiting..."
sleep 2

ngrok_url=""
while true; do
  tunnels=$(curl -s 'http://ngrok:4040/api/tunnels')
  ngrok_url_1=$(echo $tunnels | jq -r '.tunnels[0].public_url')
  ngrok_url_2=$(echo $tunnels | jq -r '.tunnels[1].public_url')
  if [[ $ngrok_url_1 == https* ]]
  then
    ngrok_url=$ngrok_url_1
    break
  elif [[ $ngrok_url_2 == https* ]]
  then
    ngrok_url=$ngrok_url_2
    break
  fi
done

echo "Ngrok url received: $ngrok_url"