# php-docker-boilerplate

Генерирует docker окружение

### Первый шаг

Запустить `init_v2.sh` и следовать указаниям

### Первый шаг (`Windows`)

Запуск из-под линукса

### Результат

Команда создаст проект `<ProjectPath>/<ProjectName>` с деревом каталогов:

```text
.
├── backend
│   └── public
│       └── index.php
├── docker
│   ├── cron
│   │   ├── Dockerfile
│   │   └── crontab
│   ├── mailer
│   ├── mysql
│   │   ├── Dockerfile
│   │   ├── config
│   │   │   └── my.cnf
│   │   └── logs
│   ├── nginx
│   │   ├── Dockerfile
│   │   ├── certs
│   │   ├── config
│   │   │   ├── conf.d
│   │   │   └── nginx.conf
│   │   ├── htpasswd
│   │   ├── logs
│   │   └── templates
│   │       └── backend.conf.template
│   ├── ngrok
│   ├── php
│   │   ├── Dockerfile
│   │   ├── config
│   │   │   └── my.ini
│   │   └── logs
│   ├── postgresql
│   │   └── Dockerfile
│   ├── redis
│   │   ├── Dockerfile
│   │   └── config
│   │       └── my.conf
│   └── supervisor
│       ├── Dockerfile
│       ├── config
│       │   └── supervisord.conf
│       └── logs
├── docker-compose.yml
└── .env

```

Где,

- `.idea` это сгенерированные заранее настройки для PhpStorm. Включают в себя:

```text
PHP->CLI interpretation
PHP->Servers
PHP->Test Frameworks
```

- `docker` все настройки образов
- `backend/public/index.php` - это проверочный файл для первых включений
- `.env`

Команда заранее спросит какие образы необходимо добавить и уже с учетом этих образов создаст древо каталогов.

В текущем варианте, порты всех образов проставляются по умолчанию (прим. nginx:80, mysql:3306, итд).

### Образы

#### Fluent (logger)

Перенаправить логи докера в `fluent`. Версия docker compose повышается до `3.4`.

Есть некоторые отличия в запуске `docker compose up`.
Контейнеры с `logging: *default-logging` зависят от `fluent` и его нужно запустить в первую очередь `docker compose up fluent`. 
Дальше запускаются остальные контейнеры.

#TODO перевести все логи сервисов в `stdout`

Полезности:

- [Docker extension fields](https://docs.docker.com/compose/compose-file/compose-file-v3/#extension-fields)

- [Docker fluentd logger driver](https://docs.docker.com/config/containers/logging/fluentd/)

- [Fluent docker logging](https://www.fluentd.org/guides/recipes/docker-logging)

- [Fluent docker image](https://hub.docker.com/r/fluent/fluentd/)

#### Mailer

[inbucket](https://inbucket.org/)[[3.0.1-rc2](https://hub.docker.com/layers/inbucket/inbucket/3.0.1-rc2/images/sha256-d8e1dda8f7569202c810efa3cce2adead147f22a97640b0b1f8e4b3a10640bf4?context=explore)]

Локальный smtp сервер для получения почты с приложения

#### Ngrok (TODO)

[ngrok](https://ngrok.com/)[[3](https://hub.docker.com/layers/ngrok/ngrok/3/images/sha256-535d7fbadaacce01c511a95f1cd1dd3ab7795fb2164ed00d4c95dd094ba06770?context=explore)]

Сделать видимым приложение в интернете

#### Cron

Задачи добавляются в файле `crontab`:

```text
* * * * * cd /var/www && php artisan schedule:run >> /dev/stdout 2>&1
```

#### Supervisor

Задачи добавляются в файле `supervisord.conf`:

```text
[program:laravel-worker]
process_name=%(program_name)s_%(process_num)02d
command=php /var/www/artisan queue:work
autostart=true
autorestart=true
stopasgroup=true
killasgroup=true
numprocs=1
redirect_stderr=true
stdout_logfile=/var/www/storage/logs/worker.log
stopwaitsecs=3600
stdout_logfile_maxbytes=5MB
```

### Запуск

Билд `docker-compose build`

Запуск `docker-compose up -d`

### Фреймворк

- Зайти в контейнер `docker compose exec backend bash` или `docker compose exec backend bash -c "composer create-project <фрейморк>"`
- Очистить папку `backend` 
- Фреймворки:
    - [Laravel](https://laravel.com/docs/master/installation#your-first-laravel-project) `composer create-project --prefer-dist laravel/laravel .`
    - [Symfony](https://symfony.com/doc/current/setup.html#creating-symfony-applications) `composer create-project symfony/skeleton:"6.1.*" .`
    - `composer create-project --prefer-dist laravel/lumen .`
    - `composer create-project codeigniter4/appstarter `
    - `composer create-project laminas-api-tools/api-tools-skeleton .`
    - `composer create-project --prefer-dist yiisoft/yii2-app-basic .`

Некоторые настройки `.env` фреймворков:

- Laravel:

```dotenv
DB_CONNECTION=postgresql
DB_HOST=database
DB_PORT=5432
DB_DATABASE=${DB}
DB_USERNAME=${DB_USER}
DB_PASSWORD=${DB_PASSWORD}

MAIL_MAILER=smtp
MAIL_HOST=mailer
MAIL_PORT=2500
```

- Symfony:

```dotenv
DATABASE_URL="postgresql://${DB_USER}:${DB_PASSWORD}@database:5432/${DB}?serverVersion=15&charset=utf8"
MAILER_DSN=smtp://mailer:2500
```

### Другое

- [Возможные ошибки](./docs/errors.md)

- [PHP зависимости](./docs/dockerfile-php-alpine.md)

- [PHP Web Page](./docs/XDEBUG-php-web-page.md)
  
- [Debug Connections](./docs/XDEBUG-php-debug-connections.md)

- [mikefarah/yq](https://github.com/mikefarah/yq)[[v4.34.1](https://github.com/mikefarah/yq/releases/tag/v4.34.1)]





