#!/bin/bash

scriptPath=$(dirname "$0")

# Param: $1 string
function _toLower() {
  echo "$1" | tr '[:upper:]' '[:lower:]'
}

# Param: $1 string
function _empty() {
  if [ -n "$1" ]; then
      return 1
  fi

  return 0
}

# Param: $1 projectName
# Param: $2 projectPath
# Param: $3 services
function clone() {
  local projectName=$(_toLower "$1")
  local projectPath=$(_toLower "$2")
  local availableServicesArray=($3)
  mkdir -p "$projectPath"
  mkdir -p "$projectPath/$projectName"
  mkdir -p "$projectPath/$projectName/backend"
  mkdir -p "$projectPath/$projectName/docker"

  for service in "${availableServicesArray[@]}"; do
      cp -r "$scriptPath/docker/$service/" "$projectPath/$projectName/docker/$service/"
  done
}

# Param: $1 projectName
# Param: $2 projectPath
function clear() {
  local projectName=$(_toLower "$1")
  local projectPath=$(_toLower "$2")
  find "$projectPath/$projectName/docker/" -name "docker-compose.yml" -type f -delete
  rm "$projectPath/$projectName/docker-compose.tmp.yml"
}

function getAvailableServices() {
  local availableServicesArray=($(find "$scriptPath/docker" -maxdepth 1 -mindepth 1 -type d -exec basename {} \;))
  echo "${availableServicesArray[@]}"
}

# Param: $1 projectName
# Param: $2 projectPath
# Param: $3 services
function createDockerComposeFile() {
  local projectName=$(_toLower "$1")
  local projectPath=$(_toLower "$2")
  local availableServicesArray=($3)
  local files="$scriptPath/docker/docker-compose.yml"

  for service in "${availableServicesArray[@]}"; do
    files="${files} $scriptPath/docker/$service/docker-compose.yml"
  done

  # https://github.com/mikefarah/yq
  # https://github.com/mikefarah/yq/releases/tag/v4.34.1
  # MacOS
  if [[ "$(uname -s)" == 'Darwin' ]]; then
    yq=yq_darwin_amd64
  else
    yq=yq_linux_amd64
  fi

  ./$yq eval-all '. as $item ireduce ({}; . * $item)' \
    $files \
    > "$projectPath/$projectName/docker-compose.tmp.yml"
}

# Param: $1 projectName
# Param: $2 projectPath
function updateDockerComposeFile() {
  local projectName=$(_toLower "$1")
  local projectPath=$(_toLower "$2")
  local stub=$(cat "$projectPath/$projectName/docker-compose.tmp.yml")

  ###########
  # NETWORK #
  ###########
  stub=${stub//\$NETWORK\$/"network-$projectName"}
  ###########
  # BACKEND #
  ###########
  stub=${stub//\$BACKEND\$/"backend-$projectName"}

  echo "$stub" > "$projectPath/$projectName/docker-compose.yml"
}

# Param: $1 projectName
# Param: $2 projectPath
function createPhpStormConfigFromStub() {
  local projectName=$(_toLower "$1")
  local projectPath=$(_toLower "$2")
  mkdir -p "$projectPath/$projectName/.idea/"

  local stub=$(cat "$scriptPath/stubs/phpstorm/workspace.xml.stub")
  stub=${stub//\$SERVER_NAME\$/"backend-$projectName"}
  stub=${stub//\$SERVER_PORT\$/"80"}
  echo "$stub" > "$projectPath/$projectName/.idea/workspace.xml"

  stub=$(cat "$scriptPath/stubs/phpstorm/php.xml.stub")
  stub=${stub//\$INTERPRETER_ID\$/"436e9ee3-805e-49c9-bdf1-82ad87f76d64"}
  echo "$stub" > "$projectPath/$projectName/.idea/php.xml"

  stub=$(cat "$scriptPath/stubs/phpstorm/php-test-framework.xml.stub")
  stub=${stub//\$INTERPRETER_ID\$/"436e9ee3-805e-49c9-bdf1-82ad87f76d64"}
  echo "$stub" > "$projectPath/$projectName/.idea/php-test-framework.xml"
}

# Param: $1 projectName
# Param: $2 projectPath
function createHelloWorldFromStub() {
  local projectName=$(_toLower "$1")
  local projectPath=$(_toLower "$2")
  local stub=$(cat "$scriptPath/stubs/backend/hello-world.stub")

  mkdir -p "$projectPath/$projectName/backend/public"
  echo "$stub" > "$projectPath/$projectName/backend/public/index.php"
}

# Param: $1 projectName
# Param: $2 projectPath
function createEnvFromDockerComposeFile() {
  local projectName=$(_toLower "$1")
  local projectPath=$(_toLower "$2")

  variables=$(grep -o '\${[^}]*}' "$projectPath/$projectName/docker-compose.yml" | sed 's/${\(.*\)}/\1=/')
  echo "$variables" > "$projectPath/$projectName/.env"
}

read -p "Project name: " projectName
if _empty "$projectName"; then echo "[Error] Project name can't be empty"; exit 1; fi

read -p "Project path: " projectPath
if _empty "$projectPath"; then echo "[Error] Project path can't be empty"; exit 1; fi

availableServices=$(getAvailableServices)
echo "Available project services: [$availableServices]"

read -p "Project services [* - for all services]: " projectServices
if _empty "$projectServices"; then echo "[Error] Project services can't be empty"; exit 1; fi

if [[ " $projectServices " =~ " * " ]]; then
    projectServices=$availableServices
elif [[ ! " ${availableServices[*]} " =~ " $projectServices " ]]; then
    echo "[Error] Services [$projectServices] not fount in available project services [$availableServices]"
    exit 1
fi

clone "$projectName" "$projectPath" "$projectServices"
createDockerComposeFile "$projectName" "$projectPath" "$projectServices"
updateDockerComposeFile "$projectName" "$projectPath"
createEnvFromDockerComposeFile "$projectName" "$projectPath"
createPhpStormConfigFromStub "$projectName" "$projectPath"
createHelloWorldFromStub "$projectName" "$projectPath"
clear "$projectName" "$projectPath"

echo "Created: $projectPath/$projectName"
echo "Services: $projectServices"
echo "All ports have a default value. (3306, 80, etc)"
