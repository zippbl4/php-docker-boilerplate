```dockerfile
RUN apk add --no-cache \
    curl \
    bash \
    nano \
    git \
    $PHPIZE_DEPS \
    && apk add --update linux-headers \
``` 

```dockerfile
RUN apk add libzip-dev zip \
    && docker-php-ext-install zip
```

```dockerfile
RUN apk add libpq-dev \
    && docker-php-ext-install pdo_pgsql
```

```dockerfile
RUN apk add icu icu-dev \
    && docker-php-ext-configure intl \
    && docker-php-ext-install intl
```

```dockerfile
RUN apk add libpng-dev libwebp-dev libjpeg-turbo-dev freetype-dev \
    && docker-php-ext-configure gd --enable-gd --with-freetype --with-jpeg --with-webp \
    && docker-php-ext-install gd
```

```dockerfile
RUN apk add libxslt libxslt-dev  \
    && docker-php-ext-install xsl
```

```dockerfile
RUN docker-php-ext-install pdo
```

```dockerfile
RUN apk add oniguruma oniguruma-dev \
    && docker-php-ext-install mbstring
```

```dockerfile
RUN curl -sS https://getcomposer.org/installer | php -- \
    --install-dir=/usr/bin \
    --filename=composer
```

```dockerfile
RUN curl -sS https://get.symfony.com/cli/installer | bash \
    && mv /root/.symfony5/bin/symfony /usr/bin/symfony
```

```dockerfile
RUN pecl update-channels \
    && pecl install xdebug \
    && docker-php-ext-enable xdebug 
# OR
RUN set -ex && apk add --no-cache --virtual .xdebug-deps $PHPIZE_DEPS \
    pecl update-channels && pecl install xdebug \
    docker-php-ext-enable xdebug \
    apk del .xdebug-deps
```

```dockerfile
RUN pecl update-channels \
    && pecl install xhprof \
    && docker-php-ext-enable xhprof 
# OR
RUN set -ex && apk add --no-cache --virtual .xhprof-deps $PHPIZE_DEPS \
    pecl update-channels && pecl install xhprof \
    docker-php-ext-enable xhprof \
    apk del .xhprof-deps
```