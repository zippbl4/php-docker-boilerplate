### 1

![](./errors/linux-headers.jpg)

добавить в `Dockerfile`:

```dockerfile
RUN apk add --update linux-headers
```

### 2

![](./errors/pecl_error.jpg)

изменить `Dockerfile` c:

```dockerfile
RUN pecl update-channels && pecl install xhprof
```

на:

```dockerfile
RUN curl "http://pecl.php.net/get/xhprof-2.3.5.tgz" -fsL -o ./xhprof-2.3.5.tgz && \
    mkdir /var/xhprof && tar xf ./xhprof-2.3.5.tgz -C /var/xhprof && \
    cd /var/xhprof/xhprof-2.3.5/extension && \
    phpize && \
    ./configure && \
    make && \
    make install
```

### 3

![](./errors/pecl_error.jpg)

изменить `Dockerfile` c:

```dockerfile
RUN pecl update-channels && pecl install xhprof
```

на:

```dockerfile
RUN curl "http://pecl.php.net/get/xhprof-2.3.5.tgz" -fsL -o ./xhprof-2.3.5.tgz && \
    mkdir /var/xhprof && tar xf ./xhprof-2.3.5.tgz -C /var/xhprof && \
    cd /var/xhprof/xhprof-2.3.5/extension && \
    phpize && \
    ./configure && \
    make && \
    make install
```