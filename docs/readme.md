# Первое включение

#### Linux/MacOS

Запустить `init.sh` ввести имя проекта, порт для XHPROF, порт для NGINX, порт для MYSQL.

#### Общее

Если конфликтов портов не предвидится:

- XHPROF - любой, я ставлю 49999

- NGINX - 80

- MYSQL - 3306

Комманда сгенерирует `docker-compose.yml` который нужно будет запустить.

Билдим контейнер `docker-compose build`

Запускаем контейнер в фоне `docker-compose up -d`

# Логи

### PHP

Пробрасываются и пишутся логи (`./docker/php/logs/:/var/log/php/`):

- **php** (`php.ini`)
```ini
error_log=/var/log/php/php.log
```
- **xdebug** (`php.ini`)
```ini
xdebug.log=/var/log/php/xdebug.log
```
- **xhprof** (непосредственно в коде. раздел **XHPROF**)
```php
$filename = "/var/log/php/$run.$type.xhprof";
```

# XHPROF

Установка в 2 шага:

- `dockerfile` - XHProf
- `docker-compose` - XHProfUI

### Описание `dockerfile`

```dockerfile
RUN set -ex && apk add --no-cache --virtual .xhprof-deps $PHPIZE_DEPS
RUN pecl update-channels && pecl install xhprof
RUN docker-php-ext-enable xhprof
RUN apk del .xhprof-deps
```

### Описание `docker-compose.yaml`

```yaml
xhprof:
    image: tuimedia/xhprof-docker:0.9.4
    volumes:
      - ./backend/logs/php/:/profiles/
    ports:
      - "49998:80"
```

### Использование

```php
define("XHPROF_DEBUG", extension_loaded("xhprof"));
define("XHPROF_TYPE", "projectName");

if (XHPROF_DEBUG) {
    xhprof_enable(XHPROF_FLAGS_CPU + XHPROF_FLAGS_MEMORY);
    register_shutdown_function(function () {
        $run = time();
        $type = XHPROF_TYPE;
        $filename = "/var/log/php/$run.$type.xhprof";
        file_put_contents($filename, serialize(xhprof_disable()));
    });
}
```

### Анализ

[XHProfUI](http://localhost:49998/)

# XDEBUG

### Описание `dockerfile`:

```dockerfile
RUN set -ex && apk add --no-cache --virtual .xdebug-deps $PHPIZE_DEPS
RUN pecl update-channels && pecl install xdebug
RUN docker-php-ext-enable xdebug
RUN apk del .xdebug-deps
```

Настройки:

- [Ошибки](./errors.md)

- [PHP Web Page](./XDEBUG-php-web-page.md)
  
- [Debug Connections](./XDEBUG-php-debug-connections.md)





