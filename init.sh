#!/bin/bash

SCRIPT_PATH=$(dirname "$0")
CURRENT_PATH=$(pwd)

# Param: $1 string
function _toLower() {
  local str=$1
  echo $str | tr '[:upper:]' '[:lower:]'
}

# Param: $1 project
function exists() {
  local project=$(_toLower $1)

  if [ -d "$project" ] && [ ! -L "$project" ]; then
    return 0
  fi

  return 1
}

# Param: $1 project
function clone() {
  local project=$(_toLower $1)

  mkdir $project
  mkdir -p $project/backend
  cp -r $SCRIPT_PATH/docker $project/docker
}

# Param: $1 project
# Param: $2 nginxPort
# Param: $3 mysqlPort
# Param: $4 xhprofPort
# Param: $5 serverName
function createDockerComposeFromStub() {
  local project=$(_toLower $1)
  local stub=$(cat $SCRIPT_PATH/stubs/docker/docker-compose-linux.stub)
  ###########
  # NETWORK #
  ###########
  stub=${stub//\$NETWORK\$/"network-$project"}
  ###########
  # BACKEND #
  ###########
  local serverName=$5
  stub=${stub//\$BACKEND\$/"backend-$project"}
  stub=${stub//\$UID\$/"$(id -u)"}
  stub=${stub//\$GID\$/"$(id -g)"}
  stub=${stub//\$UNAME\$/"$(id -nu)"}
  stub=${stub//\$IDE_SERVER_NAME\$/"$serverName"}
  #########
  # NGINX #
  #########
  local nginxPort=$2
  stub=${stub//\$NGINX_PORT\$/"$nginxPort"}
  stub=${stub//\$NGINX\$/"nginx-$project"}
  ##########
  # XHPROF #
  ##########
  local xhprofPort=$4
  stub=${stub//\$XHPROF\$/"xhprof-$project"}
  stub=${stub//\$XHPROF_PORT\$/"$xhprofPort"}
  #########
  # MYSQL #
  #########
  local mysqlPort=$3
  local dbDatabase="$project-app"
  local dbUsername="$project-user"
  local dbPassword="$project-pass"
  stub=${stub//\$MYSQL\$/"mysql-$project"}
  stub=${stub//\$MYSQL_PORT\$/"$mysqlPort"}
  stub=${stub//\$DB_DATABASE\$/"$dbDatabase"}
  stub=${stub//\$DB_USERNAME\$/"$dbUsername"}
  stub=${stub//\$DB_PASSWORD\$/"$dbPassword"}

  echo "$stub" > $project/docker-compose.yml
}

function getBackendFromStub() {
  sed -i -ne '/<!-- BEGIN realm -->/ {p; r realm.xml' -e ':a; n; /<!-- END realm -->/ {p; b}; ba}; p' server.xml
}

# Param: $1 project
# Param: $2 nginxPort
# Param: $3 serverName
function createPhpStormConfigFromStub() {
  local project=$(_toLower $1)
  local nginxPort=$2
  local serverName=$3
  mkdir -p $project/.idea/

  local stub=$(cat "$SCRIPT_PATH/stubs/phpstorm/workspace.xml.stub")
  stub=${stub//\$SERVER_NAME\$/"$serverName"}
  stub=${stub//\$SERVER_PORT\$/"$nginxPort"}
  echo "$stub" > "$project/.idea/workspace.xml"

  stub=$(cat "$SCRIPT_PATH/stubs/phpstorm/php.xml.stub")
  stub=${stub//\$INTERPRETER_ID\$/"436e9ee3-805e-49c9-bdf1-82ad87f76d64"}
  echo "$stub" > "$project/.idea/php.xml"

  stub=$(cat "$SCRIPT_PATH/stubs/phpstorm/php-test-framework.xml.stub")
  stub=${stub//\$INTERPRETER_ID\$/"436e9ee3-805e-49c9-bdf1-82ad87f76d64"}
  echo "$stub" > "$project/.idea/php-test-framework.xml"
}

# Param: $1 project
function createHelloWorldFromStub() {
  local project=$(_toLower $1)
  local stub=$(cat $SCRIPT_PATH/stubs/backend/hello-world.stub)

  mkdir -p $project/backend/public
  echo "$stub" > $project/backend/public/index.php
}

# Param: $1 project
function createEnvFromStub() {
  local project=$(_toLower "$1")
  local stub=$(cat "$SCRIPT_PATH/stubs/docker/env.stub")

  stub=${stub//\$UID\$/"$(id -u)"}
  stub=${stub//\$GID\$/"$(id -g)"}
  stub=${stub//\$UNAME\$/"$(id -nu)"}
  stub=${stub//\$BACKEND\$/"backend-$projectName"}

  echo "$stub" > "$project/.env"
}

read -p "Target dir:        " project
read -p "NGINX PORT:        " nginxPort
read -p "MYSQL PORT:        " mysqlPort
read -p "XHPROF PORT:       " xhprofPort
read -p "[DEBUG] ServerName:" serverName

if $(exists "$project"); then
  exit 0
fi

clone "$project"
createDockerComposeFromStub "$project" "$nginxPort" "$mysqlPort" "$xhprofPort" "$serverName"
createPhpStormConfigFromStub "$project" "$nginxPort" "$serverName"
createHelloWorldFromStub "$project"
createEnvFromStub "$project"
